3409390.3409410.pdf
	Papier sur implémentation directement dans slurm de scheduler pour allouer des jobs et minimiser les communications sur les différentes nodes (pas les différent threads, seulement les nodes).
	"5.2 Implementation Details
	We modify the data structures and functions related to the se-
	lect/linear plugin of SLURM for implementing our algorithms. We
	use the environment variable JOBAWARE to execute our proposed
	algorithms. When JOBAWARE is defined, SLURM executes our pro-
	posed algorithm, else it executes the default allocation. The pro-
	posed algorithms have negligible overhead (less than 0.1 second).
	The network topology information is provided in a topology.conf
	file. For each leaf switch, the topology.conf file lists the nodes con-
	nected to it, and for the higher level switches, it contains a list
	of the underlying (child) switches. We obtained fat-tree topology
	files from IIT Kanpur and Lawrence Berkeley National Laboratory.
	The former has 16 nodes/leaf switch, whereas the latter has ≥ 300
	nodes/leaf switch. An example topology.conf file for the tree topol-
	ogy of Figure 2 is shown below.
	SwitchName=s0 Nodes=n[0-3]
	SwitchName=s1 Nodes=n[4-7]
	SwitchName=s2 Switches=s[0-1]"
	Communications pendant les jobs. 

ajout_algo_simulateur_slurm.pdf
	Implémentation d'un simulateur de slurm.

Staged_memory_scheduling_Achieving_high_performance_and_scalability_in_heterogeneous_systems.pdf
	CPU et GPU qui partage la même mémoire principale. Le papier propose de découper la mémoire en 3 partie
	pour améliorer la fairness entre CPU et GPU en ordonnant les taches avec un scheduler. Le but st de minimiser
	les interferences causé par les applis GPU. C'est plus application-aware.

3-540-63574-2_24.pdf : Improved Utilization and Responsiveness with Gang Scheduling
	Dileme de : est-ce que schedule un petit job alors que des gros jobs pourrait arriver derrière ?
	Ou alors devrais je le laisser idle ? Ajout de gang scheduling et de time slice pour répondre à ce problème.
	Le dileme est plus formellement : 
	"- if the new job can be accommodated, then scheduling it immediately will
	utilize unused resources, so it is good.
	- however, if this job runs for a long time, and will block other jobs in the
	future, it may lead to more future loss than current gain. So maybe it should
	be left aside."
	Ils vont donc diviser le temps alloué à un tache :
	"The preferred solution is to use gang scheduling [22,7,8,27]. With gang schedul-
	ing, jobs receive the number of processors requested, but only for a limited time
	quantum. Then a "multi-context-switch" is performed on all the processors at
	once, and another job (or set of jobs) is scheduled instead. Thus all jobs can
	execute concurrently using time slicing, as in conventional uniprocessors. As a
	result, a scheduling decision only impacts the scheduling slot to which it per-
	tains; other slots are available to handle other jobs and future arrivals. This adds
	flexibility and boosts performance."

Dynamic_Fractional_Resource_Scheduling_versus_Batch_Scheduling.pdf
	"use of virtual machine technology to share fractional node resources in a precise and controlled manner."
	Suppose que toute les tâches on la même demandede mémoire. Il ne ré utilise pas la mémoire mais a la place
	on un systeme de priorité pour gérer le fait que un job pourrait ne pas rentrer en mémoire.
