gonthier@gonthier-Precision-3551:~/data-aware-batch-scheduling/MBSS$ python3 src/get_nuser_from_converted_workload.py 
Reading inputs/workloads/converted/2022-02-21->2022-02-27_V10000
Reading inputs/workloads/converted/2022-07-11->2022-07-17_V10000
Reading inputs/workloads/converted/2022-09-05->2022-09-11_V10000
Reading inputs/workloads/converted/2022-10-10->2022-10-16_V10000
Reading inputs/workloads/converted/2022-01-10->2022-01-16_V10000
Reading inputs/workloads/converted/2022-02-07->2022-02-13_V10000
Reading inputs/workloads/converted/2022-03-28->2022-04-03_V10000
Reading inputs/workloads/converted/2022-10-17->2022-10-23_V10000
Reading inputs/workloads/converted/2022-10-24->2022-10-30_V10000
Reading inputs/workloads/converted/2022-12-12->2022-12-18_V10000
Reading inputs/workloads/converted/2022-01-03->2022-01-09_V10000
Reading inputs/workloads/converted/2022-10-03->2022-10-09_V10000

##### Stats #####:
Nb of jobs started before the evaluated week: 14921
Nb of jobs submitted before the evaluated week: 7276
Nb of scheduled jobs before the evaluated week: 218537
Nb of evaluated jobs: 1493151
Nb of scheduled jobs after the evaluated week: 252611
Total number of jobs: 1.986.496
Number of distinct users: 1083
Number of batch of users: 136404
Number of multi node jobs: 300 + 200 + 141 + 513 + 164 + 766 + 756 + 515 + 738 + 511 + 1514 + 322 = 6440 = 0.32%

%~ Core hours MAX possible is 19595520
%~ core_time in hours are:
%~ 13282427.83638889, 13280544.279722223, 13233141.786944445, 13280172.383611111, 13268665.10111111
%~ Percentages of reductions from FCFS for core_time mode NO_BF are  EFT: 0.01418081611184384 LEA: 0.37106205319948427 LEO: 0.016980726758391865 LEM: 0.10361611180806136
%~ Nb d'heures de diff pour LEA: 49286,049444445 
%~ and for transfer time:
%~ transfer_time in hours are:
%~ 76053.51472222223, 75398.39972222222, 63084.8275, 75348.51444444444, 70665.11944444444
%~ Percentages of reductions from FCFS for transfer_time mode NO_BF are  EFT: 0.8613868831608129 LEA: 17.052055081989362 LEO: 0.9269792202933947 LEM: 7.0850049435037254
