# From a converted workload, count number of multi node jobs that were divided. It's easy they have the same user, 20 cores used, the same delay walltime and start time from history and submission time.
# Only node from history and job id vary
# python3 src/get_number_of_multinode_jobs_from_converted_workload.py converted_input_file
# { id: 1 subtime: 0 delay: 388923 walltime: 388800 cores: 20 user: marvin data: 1 data_size: 128.000000 workload: -2 start_time_from_history: 3164092 start_node_from_history: 337 }

import sys
import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
from matplotlib.lines import Line2D

FILENAME = sys.argv[1]

f_input = open(FILENAME, "r")

nb_multi_node_jobs = 0
new_multi_node_job_possible = True

last_subtime = 0
last_delay = 0
last_walltime = 0
last_user = ""
last_data = 0
last_data_size = 0
last_start_time_from_history = 0

current_subtime = 0
current_delay = 0
current_walltime = 0
current_number_of_cores_used = 0
current_user = ""
current_data = 0
current_data_size = 0
current_start_time_from_history = 0

line = f_input.readline()
while line:
	r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24 = line.split()
	
	# { id: 1 subtime: 0 delay: 388923 walltime: 388800 cores: 20 user: marvin data: 1 data_size: 128.000000 workload: -2 start_time_from_history: 3164092 start_node_from_history: 337 }
		
	current_subtime = int(str(r5))
	current_delay = int(str(r7))
	current_walltime = int(str(r9))
	current_number_of_cores_used = int(str(r11))
	current_user = str(r13)
	current_data = int(str(r15))
	current_data_size = float(str(r17))
	current_start_time_from_history = int(str(r21))
	
	if (current_subtime == last_subtime and current_delay == last_delay and current_walltime == last_walltime and current_number_of_cores_used == 20 and current_user == last_user and current_data == last_data and current_data_size == last_data_size and current_start_time_from_history == last_start_time_from_history):
		if (new_multi_node_job_possible == True):
			nb_multi_node_jobs += 1
			print("multi at subtime", current_subtime, "user", current_user, "delay", current_delay)
			new_multi_node_job_possible = False
	else:
		last_subtime = current_subtime
		last_delay = current_delay
		last_walltime = current_walltime
		last_user = current_user
		last_data = current_data
		last_data_size = current_data_size
		last_start_time_from_history = current_start_time_from_history
		new_multi_node_job_possible = True
	
	line = f_input.readline()
f_input.close()

print("nb_multi_node_jobs:", nb_multi_node_jobs)
