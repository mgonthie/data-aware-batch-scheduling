\documentclass{beamer}
\input{preamble.tex}
\input{tikz-macros.tex}
\usepackage{pgfgantt}

\NewDocumentCommand\textganttbarB{O{} O{} mmmm}{%
    \ganttbar[#1,bar/.append style={alias=tmp}]{#3}{#5}{#6}
    \node [font=\footnotesize,at={(tmp)},#2]  {#4};
}

\tikzset{
  above bar/.style={
    at={(tmp.north)},anchor=south
    },
  below bar/.style={
    at={(tmp.south)},anchor=north
    }
}

% Title page with image
\title{Data-Driven Locality-Aware Batch Scheduling}
\titlebackground{Images/iStock_000061296808_Large-1.jpg}
%~ \subtitle{\textit{PhD defense}}
\author{
\textbf{Maxime GONTHIER}\\
Elisabeth Larsson, Loris MARCHAL, Carl Nettelblad and Samuel THIBAULT\\
\vspace{2mm}
\includegraphics[scale=0.065]{Images/logo_fond_blanc-5-2.jpg}
\hspace{6mm}\includegraphics[scale=0.2]{Images/inr_logo_rouge.jpg}\\
\hspace{3mm}\includegraphics[scale=0.1]{Images/Logo_LIP-300x200.png}
\hspace{9mm}\includegraphics[scale=0.024]{Images/LABRI_BIG_0.png}\\
\hspace{3mm}\includegraphics[scale=0.023]{Images/cnrs.png}
\hspace{9mm}\includegraphics[scale=0.025]{Images/logo_solharis_full2.png}\\
%~ \hspace{13mm}\includegraphics[scale=0.023]{Images/Uppsala_University_logo.png}
\hspace{3mm}\includegraphics[scale=0.023]{Images/Uppsala_University_logo.png}
\hspace{9mm}\includegraphics[scale=0.017]{Images/UChicago-logo.jpg}
}
\date{May 27, 2024}

\begin{document}

\maketitle

\footlinecolor{uicblue} % A mettre après le maketitle pour pas l'afficher sur la première page

\begin{chapter}[Images/books.jpg]{uicblue}{Context}
\end{chapter}

% Motivation
\begin{frame}
\begin{columns}
\column{0.5\textwidth}
\begin{minipage}[c][0.52\textheight][c]{\linewidth}
  \centering
  \begin{figure}\includegraphics[width=4.4cm]{Images/LocARNA_aln.jpg}\caption{\textcolor{uicblue}{Genome alignment tools}\onslide<4->{ / \green{Multiple files of hundreds of MB}}}\end{figure}
\end{minipage}
\begin{minipage}[c][0.43\textheight][c]{\linewidth}
  %~ \centering
 %~ \begin{figure}\includegraphics[width=4.4cm]{Images/windmiddleeast.png}\caption{\textcolor{uicblue}{Wind speeds simulation}\onslide<4->{ / \green{3\,000 GB}}}\end{figure}
\end{minipage}
\column{0.5\textwidth}
\begin{minipage}[c][0.52\textheight][c]{\linewidth}
  \onslide<2->{What are the 2 similarities between these scientific applications?}
  \begin{enumerate}
  \item \onslide<3->{High computational intensity}
  \item \onslide<4->{\green{Data intensive}}
  \end{enumerate}
  \onslide<5->{\textbf{$\Rightarrow$ What kind of system can compute these applications?}}
\end{minipage}
\begin{minipage}[c][0.43\textheight][c]{\linewidth}
  \centering
  \begin{figure}\includegraphics[width=4.1cm]{Images/DNA_Barcoding.png}\caption{\textcolor{uicblue}{Taxonomic identification of DNA fragments}\onslide<4->{ / \green{between 10 and 800 GB}}}\end{figure}
\end{minipage}
\end{columns}
\end{frame}

% Scheduler's role
\begin{frame}[fragile]{High-Performance Computing platforms can!}
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{itemize}
\item Set of nodes
\item Each equipped with CPUs with limited memory
\item The SLURM scheduler assigns jobs to each node
\end{itemize}
\end{column}
\begin{column}{0.5\textwidth}
\begin{figure}
\includegraphics[scale=0.19]
{Images/node_structure.pdf}
\end{figure}
\end{column}
\end{columns}
\end{frame}

% Motivation
\begin{frame}{Motivation}
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{enumerate}
	\item \onslide<1->{Widening gap between computing speed and communications bandwidth}
	\item \onslide<2->{Performance bottleneck}
\end{enumerate}
\begin{block}
\onslide<3->{\textbf{$\Rightarrow$ Minimize execution time of data-intensive applications at the scale of a supercomputer}}
\end{block}
\end{column}
\begin{column}{0.5\textwidth}
\begin{overprint}
\onslide<1>
\includegraphics[scale=0.4]{Images/gap.pdf}
\footnotesize{From: \textit{Optical interconnection networks for high-performance systems} - Cheng et al.}
\onslide<2-3>
\includegraphics[scale=0.3]{Images/bottleneck.pdf}
\end{overprint}
\end{column}
\end{columns}
\end{frame}

% Plan
\begin{frame}{Table of contents}
\begin{block}{Motivation}
Minimize execution time of data-intensive applications at the scale of a supercomputer
\end{block}
	\begin{itemize}
		\item \textcolor{gray}{Context}
		\item \onslide<2->State-of-the art schedulers
		\item \onslide<3->Proposed schedulers
		\item \onslide<4->Experimental settings
		\item \onslide<5->Experimental evaluation
	\end{itemize}
\end{frame}

\begin{chapter}[Images/gantt-chart-color-icon-illustration-vector.jpg]{uicblue}{Scheduling strategies}
\end{chapter}

% FCFS and EFT
\begin{frame}{State-of-the art schedulers: FCFS and EFT}
\setbeamercolor{block title}{bg=uicbluecontribution}
\setbeamercolor{block body}{bg=uicbluecontribution}
\begin{columns}
\begin{column}{0.49\textwidth}
\begin{block}{First-Come First-Serve}
Schedule jobs on the node with the earliest available time $t_k$
\end{block}

\scalebox{0.9}{
\begin{ganttchart}[
  hgrid, vgrid,
  group progress label node/.append style={below=3pt},
  canvas/.append style={label=below:FCFS} ]{1}{12}
\begin{scope}[yshift=0.33cm]
    \draw[-stealth] (0,0) -- (6,0);
    \draw ([yshift=-3pt]0,0) -- ++([yshift=6pt]0,0) node[above] {0};
    \draw ([yshift=-3pt]1,0) -- ++([yshift=6pt]0,0) node[above] {$2$};
    \draw ([yshift=-3pt]2,0) -- ++([yshift=6pt]0,0) node[above] {$4$};
    \draw ([yshift=-3pt]3,0) -- ++([yshift=6pt]0,0) node[above] {$6$};
    \draw ([yshift=-3pt]4,0) -- ++([yshift=6pt]0,0) node[above] {$8$};
    \draw ([yshift=-3pt]5,0) -- ++([yshift=6pt]0,0) node[above] {$10$};
\end{scope}
\ganttbar[bar/.append style={draw=black,fill=blue}]{Node 1}{1}{3}
\ganttbar[bar/.append style={draw=black,fill=red}]{}{1}{2}
\node [text=white, at={(0.5,-0.5)}] {\textbf{D1}};
\node [text=white, at={(1.25,-0.5)}] {\textbf{J1}};
\\
\ganttbar[bar/.append style={draw=black,fill=blue}]{Node 2}{1}{2}
\node [text=white, at={(0.5,-1.5)}] {\textbf{J2}};
\onslide<2->{
\ganttbar[bar/.append style={draw=black,fill=red, dash pattern=on 2pt off 2pt, line width=1pt}]{}{3}{5}
\ganttbar[bar/.append style={draw=black,fill=blue, dash pattern=on 2pt off 2pt, line width=1pt}]{}{5}{7}
\node [text=white, at={(1.5,-1.5)}] {\textbf{D1}};
\node [text=white, at={(2.75,-1.5)}] {\textbf{J4}};
}
\\
\ganttbar[bar/.append style={draw=black,fill=blue}]{Node 3}{1}{4}
\ganttbar[bar/.append style={draw=black,fill=red}]{}{1}{3}
\node [text=white, at={(0.75,-2.5)}] {\textbf{D2}};
\node [text=white, at={(1.75,-2.5)}] {\textbf{J3}};
\end{ganttchart}
}
\end{column}
\begin{column}{0.49\textwidth}
\begin{block}{Earliest Finish Times}
    Look for earliest completion time using $t_k$ + transfer time $t'_k$ at which the input file of a job $J_i$ is available on
\Node{k}
\end{block}
\scalebox{0.9}{
\begin{ganttchart}[
  hgrid, vgrid,
  group progress label node/.append style={below=3pt},
  canvas/.append style={label=below:EFT} ]{1}{12}
\begin{scope}[yshift=0.33cm]
    \draw[-stealth] (0,0) -- (6,0);
    \draw ([yshift=-3pt]0,0) -- ++([yshift=6pt]0,0) node[above] {0};
    \draw ([yshift=-3pt]1,0) -- ++([yshift=6pt]0,0) node[above] {$2$};
    \draw ([yshift=-3pt]2,0) -- ++([yshift=6pt]0,0) node[above] {$4$};
    \draw ([yshift=-3pt]3,0) -- ++([yshift=6pt]0,0) node[above] {$6$};
    \draw ([yshift=-3pt]4,0) -- ++([yshift=6pt]0,0) node[above] {$8$};
    \draw ([yshift=-3pt]5,0) -- ++([yshift=6pt]0,0) node[above] {$10$};
\end{scope}
\onslide<3->{
\ganttbar[bar/.append style={draw=black,fill=blue}]{Node 1}{1}{3}
\ganttbar[bar/.append style={draw=black,fill=red}]{}{1}{2}
\node [text=white, at={(0.5,-0.5)}] {\textbf{D1}};
\node [text=white, at={(1.25,-0.5)}] {\textbf{J1}};
\onslide<4->{\ganttbar[bar/.append style={draw=black,fill=blue, dash pattern=on 2pt off 2pt, line width=1pt}]{}{4}{6}
\node [text=white, at={(2.25,-0.5)}] {\textbf{J4}};
}
\\
\ganttbar[bar/.append style={draw=black,fill=blue}]{Node 2}{1}{2}
\node [text=white, at={(0.5,-1.5)}] {\textbf{J2}};
\\
\ganttbar[bar/.append style={draw=black,fill=blue}]{Node 3}{1}{4}
\ganttbar[bar/.append style={draw=black,fill=red}]{}{1}{3}
\node [text=white, at={(0.75,-2.5)}] {\textbf{D2}};
\node [text=white, at={(1.75,-2.5)}] {\textbf{J3}};
\end{ganttchart}
}
}
\end{column}
\end{columns}
\end{frame}

% LEA
\setbeamercolor{block title}{bg=uicblue}
\setbeamercolor{block body}{bg=uicblue}
\setbeamercolor{itemize item}{fg=white}
\setbeamercolor{itemize subitem}{fg=white}
\setbeamercolor{enumerate item}{fg=white}
\setbeamercolor{enumerate subitem}{fg=white}
\begin{frame}{Proposed locality-aware scheduler: \underline{\textbf{LEA}}}
\begin{tikzpicture}[remember picture,overlay]
	\node[anchor=north east] at (current page.north east) {\includegraphics[width=2cm]{Images/phppGEuDy.jpg}}; 
\end{tikzpicture}
\begin{columns}
\begin{column}{0.49\textwidth}
\begin{block}{Locality and Eviction Aware}
\begin{itemize}
	\item Balances node availability and data locality
	\item Ranks each node \Node{k} with:
	\begin{enumerate}
	\item Earliest available time: $t_k$;
	\item \textbf{Time needed to load the input file for the job: $t'_k - t_k$;}
	\item Size of evicted input files
\end{enumerate}
\end{itemize}
\end{block}
\end{column}
\begin{column}{0.49\textwidth}
\scalebox{0.9}{
\begin{ganttchart}[
  hgrid, vgrid,
  group progress label node/.append style={below=3pt},
  canvas/.append style={label=below:LEA} ]{1}{12}
\begin{scope}[yshift=0.33cm]
    \draw[-stealth] (0,0) -- (6,0);
    \draw ([yshift=-3pt]0,0) -- ++([yshift=6pt]0,0) node[above] {0};
    \draw ([yshift=-3pt]1,0) -- ++([yshift=6pt]0,0) node[above] {$2$};
    \onslide<1>{\draw ([yshift=-3pt]2,0) -- ++([yshift=6pt]0,0) node[above] {$4$};}
    \onslide<2-3>{\draw ([yshift=-3pt]2,0) -- ++([yshift=6pt]0,0) node[above] {$4$};}
    \onslide<4->{\draw ([yshift=-3pt]2,0) -- ++([yshift=6pt]0,0) node[above] {\textbf{4}};}
    \draw ([yshift=-3pt]3,0) -- ++([yshift=6pt]0,0) node[above] {$6$};
    \draw ([yshift=-3pt]4,0) -- ++([yshift=6pt]0,0) node[above] {$8$};
    \draw ([yshift=-3pt]5,0) -- ++([yshift=6pt]0,0) node[above] {$10$};
\end{scope}
\onslide<2>{\node[draw,align=center] at (3,1.4) {t=1 ; Next job J4 uses D1};}
\onslide<3>{\node[draw,align=center] at (3,1.4) {t=1 ; Next job J5 uses D3};}
\onslide<4>{\node[draw,align=center] at (3,1.4) {t=4 ; Next job J6 uses D4};}
\onslide<2->{
\ganttbar[bar/.append style={draw=black,fill=blue}]{Node 1}{1}{3}
\ganttbar[bar/.append style={draw=black,fill=red}]{}{1}{2}
\node [text=white, at={(0.5,-0.5)}] {\textbf{D1}};
\node [text=white, at={(1.25,-0.5)}] {\textbf{J1}};
\onslide<3->{\ganttbar[bar/.append style={draw=black,fill=blue, dash pattern=on 2pt off 2pt, line width=1pt}]{}{4}{6}
\node [text=white, at={(2.25,-0.5)}] {\textbf{J4}};
}
\\
\ganttbar[bar/.append style={draw=black,fill=blue}]{Node 2}{1}{2}
\node [text=white, at={(0.5,-1.5)}] {\textbf{J2}};
\onslide<4->{
\ganttbar[bar/.append style={draw=black,fill=red, dash pattern=on 2pt off 2pt, line width=1pt}]{}{3}{4}
\ganttbar[bar/.append style={draw=black,fill=blue, dash pattern=on 2pt off 2pt, line width=1pt}]{}{4}{5}
\node [text=white, at={(1.25,-1.5)}] {\textbf{D3}};
\node [text=white, at={(2,-1.5)}] {\textbf{J5}};
}
\onslide<5->{
\ganttbar[bar/.append style={draw=black,fill=red, dash pattern=on 2pt off 2pt, line width=1pt}]{}{6}{7}
\ganttbar[bar/.append style={draw=black,fill=blue, dash pattern=on 2pt off 2pt, line width=1pt}]{}{7}{9}
\node [text=white, at={(2.76,-1.5)}] {\textbf{D4}};
\node [text=white, at={(3.75,-1.5)}] {\textbf{J6}};
}
\\
\ganttbar[bar/.append style={draw=black,fill=blue}]{Node 3}{1}{4}
\ganttbar[bar/.append style={draw=black,fill=red}]{}{1}{3}
\node [text=white, at={(0.75,-2.5)}] {\textbf{D2}};
\node [text=white, at={(1.75,-2.5)}] {\textbf{J3}};
\end{ganttchart}
}
}
\end{column}
\end{columns}
\end{frame}

% LEO
\begin{frame}{Proposed locality-aware scheduler: \underline{\textbf{LEO}}}
\begin{tikzpicture}[remember picture,overlay]
	\node[anchor=north east] at (current page.north east) {\includegraphics[width=2cm]{Images/leo.jpeg}}; 
\end{tikzpicture}
\begin{columns}
\begin{column}{0.49\textwidth}
\begin{block}{Locality and Eviction Opportunistic}
\begin{itemize}
	\item If it finds a node that can compute the job immediately, it applies \only<1-3>{\textcolor{white}{EFT}}\only<4->{\colorbox{white}{\textbf{\textcolor{red}{\large EFT}}}}
	\item Else like \only<1,4->{\textcolor{white}{LEA}}\only<2-3>{\colorbox{white}{\textbf{\textcolor{red}{\large LEA}}}} because we assume the cluster to be saturated
\end{itemize}
\end{block}
\end{column}
\begin{column}{0.49\textwidth}
\scalebox{0.9}{
\begin{ganttchart}[
  hgrid, vgrid,
  group progress label node/.append style={below=3pt},
  canvas/.append style={label=below:LEO} ]{1}{12}
\begin{scope}[yshift=0.33cm]
    \draw[-stealth] (0,0) -- (6,0);
    \draw ([yshift=-3pt]0,0) -- ++([yshift=6pt]0,0) node[above] {0};
    \draw ([yshift=-3pt]1,0) -- ++([yshift=6pt]0,0) node[above] {$2$};
    \onslide<1>{\draw ([yshift=-3pt]2,0) -- ++([yshift=6pt]0,0) node[above] {$4$};}
    \onslide<2-3>{\draw ([yshift=-3pt]2,0) -- ++([yshift=6pt]0,0) node[above] {$4$};}
    \onslide<4->{\draw ([yshift=-3pt]2,0) -- ++([yshift=6pt]0,0) node[above] {\textbf{4}};}
    \draw ([yshift=-3pt]3,0) -- ++([yshift=6pt]0,0) node[above] {$6$};
    \draw ([yshift=-3pt]4,0) -- ++([yshift=6pt]0,0) node[above] {$8$};
    \draw ([yshift=-3pt]5,0) -- ++([yshift=6pt]0,0) node[above] {$10$};
\end{scope}
\onslide<2>{\node[draw,align=center] at (3,1.4) {t=1 ; Next job J4 uses D1};}
\onslide<3>{\node[draw,align=center] at (3,1.4) {t=1 ; Next job J5 uses D3};}
\onslide<4>{\node[draw,align=center] at (3,1.4) {t=4 ; Next job J6 uses D4};}
\onslide<2->{
\ganttbar[bar/.append style={draw=black,fill=blue}]{Node 1}{1}{3}
\ganttbar[bar/.append style={draw=black,fill=red}]{}{1}{2}
\node [text=white, at={(0.5,-0.5)}] {\textbf{D1}};
\node [text=white, at={(1.25,-0.5)}] {\textbf{J1}};
\onslide<3->{\ganttbar[bar/.append style={draw=black,fill=blue, dash pattern=on 2pt off 2pt, line width=1pt}]{}{4}{6}
\node [text=white, at={(2.25,-0.5)}] {\textbf{J4}};}
\\
\ganttbar[bar/.append style={draw=black,fill=blue}]{Node 2}{1}{2}
\node [text=white, at={(0.5,-1.5)}] {\textbf{J2}};
\onslide<4->{
\ganttbar[bar/.append style={draw=black,fill=red, dash pattern=on 2pt off 2pt, line width=1pt}]{}{3}{4}
\ganttbar[bar/.append style={draw=black,fill=blue, dash pattern=on 2pt off 2pt, line width=1pt}]{}{4}{5}
\node [text=white, at={(1.25,-1.5)}] {\textbf{D3}};
\node [text=white, at={(2,-1.5)}] {\textbf{J5}};
}
\\
\ganttbar[bar/.append style={draw=black,fill=blue}]{Node 3}{1}{4}
\ganttbar[bar/.append style={draw=black,fill=red}]{}{1}{3}
\node [text=white, at={(0.75,-2.5)}] {\textbf{D2}};
\node [text=white, at={(1.75,-2.5)}] {\textbf{J3}};
\onslide<5->{
\ganttbar[bar/.append style={draw=black,fill=red, dash pattern=on 2pt off 2pt, line width=1pt}]{}{5}{6}
\ganttbar[bar/.append style={draw=black,fill=blue, dash pattern=on 2pt off 2pt, line width=1pt}]{}{6}{8}
\node [text=white, at={(2.25,-2.5)}] {\textbf{D4}};
\node [text=white, at={(3.25,-2.5)}] {\textbf{J6}};
}
\end{ganttchart}
}
}
\end{column}
\end{columns}
\end{frame}

% LEM
\setbeamercolor{enumerate item}{fg=white}
\begin{frame}{Proposed locality-aware scheduler: \underline{\textbf{LEM}}}
\begin{tikzpicture}[remember picture,overlay]
	\node[anchor=north east] at (current page.north east) {\includegraphics[width=2cm]{Images/apollo-14-descent-stage_lm.jpg}}; 
\end{tikzpicture}
\begin{columns}
\begin{column}{0.49\textwidth}
\begin{block}{Locality and Eviction Mixed}
\begin{itemize}
	\item If each node is running at least one job $\rightarrow$ LEA
	\item Else $\rightarrow$ EFT
\end{itemize}
\end{block}
\end{column}
\begin{column}{0.49\textwidth}
\begin{block}{Differences with LEO}
\begin{enumerate}
	\item Broader consideration of a saturated cluster: LEM considers a node to be saturated even if the job running on it does not use all of its cores
	\item The strategy shift is applied to all jobs until the cluster is no longer saturated
\end{enumerate}
\end{block}
\end{column}
\end{columns}
%~ \onslide<2>{We also add backfilling to all strategies}
\end{frame}

\begin{chapter}[Images/ggb-exhibit2-3_3.jpg]{uicblue}{Performance evaluation and analysis}\end{chapter}

% Expe settings
\begin{frame}{Experimental settings}
\begin{block}{Real logs from an HPC cluster}
\begin{itemize}
	\item Simulate a set of nodes from the Swedish National Infrastructure for Computing
	\item Served half of all PIs + three Nature papers
	\item 486 nodes with two 10-core Intel Xeon V4 CPU - \textbf{128\,GB}
	\item Often used for data-instensive jobs \textbf{(well over 128\,GB)}
	\item Use historical logs of jobs submitted on this cluster in 2022 (subtime, walltime, length, user)
\end{itemize}
\end{block}
\setbeamercolor{enumerate item}{fg=uicblue}

\begin{columns}
\begin{column}{0.49\textwidth}
Two jobs share an input file if:
\begin{enumerate}
	\item Same number of cores;
	\item Same user;
	\item Submitted within 800\,seconds of each other.
\end{enumerate}
\end{column}
\begin{column}{0.49\textwidth}
\vspace{-1cm}
\begin{figure}[tb]
\definecolor{blue}{rgb}{0.38, 0.51, 0.71} %glaucous, 97,130,181, #6182B5
\definecolor{darkblue}{RGB}{17, 42, 60} % 112A3C
\definecolor{red}{RGB}{175, 49, 39} % AF3127
\definecolor{otherred}{RGB}{171,78,78}
\definecolor{orange}{RGB}{237, 126, 75} 
\definecolor{green}{RGB}{104, 174, 89} 
\definecolor{palegreen}{RGB}{197, 184, 104} 
\definecolor{yellow}{RGB}{250, 199, 70} % FAC764
\definecolor{brokenwhite}{RGB}{218, 192, 166} % DAC0A6
\definecolor{brokengrey}{rgb}{0.77, 0.76, 0.82} % {196,194,209}, C4C2D1
\centering\scalebox{0.7}{
\begin{tikzpicture}[semithick, > = {Stealth[scale=1.25]}, shorten > = 1pt,]
    \def\x{1}\def\y{1}
    \draw[color=black, fill=blue] (1.2*0*\x+0.03, 0.7) rectangle (1.2*1*\x-0.03, -0.1) node[midway] {\textit{Day -n}}; 
    \draw[color=black, fill=blue] (1.2*1*\x+0.03, 0.7) rectangle (1.2*2*\x-0.03, -0.1) node[midway] {\textit{...}}; 
    \draw[color=black, fill=blue] (1.2*2*\x+0.03, 0.7) rectangle (1.2*3*\x-0.03, -0.1) node[midway] {\textit{Day -1}}; 
    \draw[color=black, fill=blue] (1.2*3*\x+0.03, 0.7) rectangle (1.2*4*\x-0.03, -0.1) node[midway] {\textit{Day 0}}; 
    \draw[color=black, fill=red] (1.2*4*\x+0.03, 0.7) rectangle (1.2*6*\x-0.03, -0.1) node[midway] {\textit{Day 1 to Day 7}};
    \draw[color=black, fill=blue] (1.2*6*\x+0.03, 0.7) rectangle (1.2*7*\x-0.03, -0.1) node[midway] {\textit{Day 8}}; 
    \draw [pen colour={blue},decorate,line width=2pt,decoration = {calligraphic brace,raise=-2pt,amplitude=5pt}] (1.2*0*\x+0.03, 1) -- node[pos=0.5,above=2pt,black,align=center]{Jobs scheduled as in the log} (1.2*3*\x-0.03, 1);
    \draw [pen colour={blue},decorate,line width=2pt,decoration = {calligraphic brace,raise=-9pt,amplitude=5pt}] (1.2*3*\x+0.03, 2.4) -- node[pos=0.5,above=2pt,black,align=center]{Jobs scheduled with the desired scheduler} (1.2*7*\x-0.03, 2.4);
    \draw [pen colour={red},decorate,line width=2pt,decoration = {calligraphic brace,raise=-2pt,amplitude=5pt}] (1.2*4*\x+0.03, 1) -- node[pos=0.5,above=2pt,black,align=center]{Evaluated\\week} (1.2*6*\x-0.03, 1);
  \end{tikzpicture}
}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\setbeamercolor{itemize item}{fg=uicblue}
\setbeamercolor{itemize subitem}{fg=uicblue}
\begin{frame}{Evaluated metrics}
\begin{itemize}
	\item Users want to complete their jobs as quickly as possible \onslide<2->{$\rightarrow$ $\emph{\mathit{Flow}}$:
$$
\mathit{Flow}(J_i) = \completiontime(J_i) - \submissiontime(J_i).
$$
}
\onslide<3->{
\item Jobs with same $\mathit{Flow}$ but different durations do not experience the same quality
of service} \onslide<4->{$\rightarrow$ $\emph{stretch}$:
\begin{eqnarray*}
\emptyflow(J_i) &=& \frac{\size(\file(J_i))}{\bandwidth} + \duration(J_i)\\
\mathit{stretch}(J_i) &=& \frac{\mathit{Flow}(J_i)}{\emptyflow(J_i)}
\end{eqnarray*}
}
\onslide<5->{
\item A user can submit a batch of jobs at once} \onslide<6->{$\rightarrow$ $\emph{\us}$}
\end{itemize}

\onslide<7->{
\begin{block}{}
\center\textbf{We focus on the stretch of each $\us$}
\end{block}
}
\end{frame}

\begin{frame}{Results on an underutilized cluster}
\onslide<1->{\begin{figure}[t]\centering\includegraphics[width=0.37\linewidth]{../MBSS/plot/Cluster_usage/2022-10-03->2022-10-09_V10000_anonymous_Fcfs_Used_nodes_450_128_32_256_4_1024_core_by_core.pdf}\end{figure}}
\onslide<2->{\begin{figure}[t]\centering\includegraphics[width=0.5\linewidth]{../MBSS/plot/Boxplot/byuser/small_hist_stretch_10-03-10-09.pdf}\caption{User session stretch compared to FCFS in a low utilization case. Numbers = mean speed-up/slow-down}\end{figure}}
\end{frame}

\begin{frame}{Results on a saturated cluster}
\onslide<1->{\begin{figure}[t]\centering\includegraphics[width=0.37\linewidth]{../MBSS/plot/Cluster_usage/2022-10-24->2022-10-30_V10000_anonymous_Fcfs_Used_nodes_450_128_32_256_4_1024_core_by_core.pdf}\end{figure}}
\center\onslide<2->{\begin{figure}[t]\centering\includegraphics[width=0.5\linewidth]{../MBSS/plot/Boxplot/byuser/small_hist_stretch_10-24-10-30.pdf}\caption{User session stretch compared to FCFS in a high utilization case. Numbers = mean speed-up/slow-down}\end{figure}}
\end{frame}

\begin{frame}{Full workload results: 2 million jobs over 12 weeks}
\only<1>{\begin{figure}\centering\includegraphics[width=0.64\linewidth]{../MBSS/plot/Boxplot/byuser/box_plot_stretch_all-all_1.pdf}\caption{Stretch's improvement from FCFS. Upper whisker of LEA extends up to 5.}\end{figure}}
\only<2>{\begin{figure}\centering\includegraphics[width=0.64\linewidth]{../MBSS/plot/Boxplot/byuser/box_plot_bf_stretch_all-all_1.pdf}\caption{Stretch's improvement from FCFS \textbf{with backfilling}. Upper whisker of LEA extends up to 4.}\end{figure}}
\end{frame}


\begin{frame}{Summary}
\setbeamercolor{itemize item}{fg=white}
\begin{block}{Limiting data movements is crucial for HPC clusters}
\begin{itemize}
	\item New locality-aware schedulers: LEA, LEO and LEM
	\item Use \textbf{system saturation}, \textbf{data re-use} and \textbf{data eviction}
\end{itemize}
\end{block}
\begin{block}{Evaluated with real logs from an HPC cluster}
\begin{itemize}
	\item \textbf{LEA}: Better for 75\% of jobs, but with non-negligible slowdowns for the other 25\%
	\item \textbf{LEM}: Best overall solution, median improvement of 7.5\% across all jobs
\end{itemize}
\end{block}
\end{frame}

% Perspectives
\setbeamercolor{itemize item}{fg=white}
\begin{frame}{Perspectives \onslide<6->{~~~~~~~~~~~~~~~~~~~~~~~~~|\textbf{mgonthier@uchicago.edu}|}}
\begin{block}{}
\begin{itemize}
	\item \onslide<2->{Gradual switch of strategy for LEM}
	\item \onslide<3->{Fairness}
	\item \onslide<4->{Workflow scheduling}
	\item \onslide<5->{Integrate these strategies into real cluster schedulers}
\end{itemize}
\end{block}
\onslide<2->{\includegraphics[width=3.3cm]{Images/4283362.png}}
\onslide<3->{\includegraphics[width=3.3cm]{Images/pngtree-simple-equal-balance-scale-icon-vector-logo-template-png-image_5207469.jpg}}
\onslide<4->{\includegraphics[width=3.3cm]{Images/workflow_output.pdf}}
\onslide<5->{\includegraphics[width=3.3cm]{Images/msingleton_180612_2663_0006.jpg}}
\end{frame}


% Joker slides
\end{document}
